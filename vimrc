" overrides vim's italic codes
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"

" install plugin manager if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" register plugins
call plug#begin('~/.vim/bundle')
Plug 'jacoborus/tender.vim'
Plug 'preservim/nerdtree'
Plug 'reewr/vim-monokai-phoenix'
Plug 'sickill/vim-monokai'
Plug 'vim-scripts/DoxygenToolkit.vim'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
call plug#end()

" run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source ~/.vim/vimrc
\| endif

colorscheme tender

" -- GLOBALS --

set encoding=utf-8    " set the file encoding to UTF-8

"enable automatic folding (folds open by default)
"@NOTE: close-open all: zM / zR | close-open fold region: zc / zo
set foldmethod=syntax foldlevel=42   " enable syntax-based folding and set the initial fold level to 42

set pastetoggle=<F3>  " set the key to toggle paste mode on and off
set autoindent        " enable automatic indentation
set cindent           " enable C-style indentation
set tabstop=2         " set the number of visual spaces per tab to 2
set shiftwidth=2      " set the number of spaces to use for auto-indentation to 2
set expandtab         " use spaces instead of tabs for indentation
set hidden            " allow hiding of unsaved buffers without saving them
set number            " display line numbers
set ruler             " display cursor position and size of the file
set showcmd           " display partial commands in the last line of the screen
set nobackup          " do not create a backup file
set nowritebackup     " do not create a backup file when overwriting a file
set noswapfile        " disable creation of swap files to avoid clutter in the directory -- NERDTREE --

" toggle nerdtree window, or create a new one if it doesn't exist yet
function! ToggleFileExplorer()
	let l:state = bufwinnr(bufnr("NERD_tree_1"))
	" when ntree window is visible and there's more than one window, we can
	" safely close the ntree window (it prevents opening another ntree window
	" when vim starts with a directory argument)
	if state == 1 && winnr('$') > 1
		NERDTreeToggle
	" when the ntree window does not exist or is not visible
	elseif state == -1
		NERDTreeToggle
	endif
endfunction
command! ToggleFileExplorer call ToggleFileExplorer()

" toggle nerdtree with ctrl+b
nnoremap <silent> <C-b> :ToggleFileExplorer<CR>

" open nerdtree in the current directory when vim starts with no arguments
autocmd VimEnter * if argc() == 0 | call ToggleFileExplorer() | only | endif

" open files in new vertical split, move cursor to it, reuse any already
" opened buffer and keep NERDTree open
let g:NERDTreeCustomOpenArgs = {'file': {'reuse': 'all','where': 'v','keepopen': 1}, 'dir': {}}
" show hidden files
let g:NERDTreeShowHidden = 1

" refresh the current folder if any changes
autocmd BufEnter NERD_tree_* | execute 'normal R'
au CursorHold * if exists("t:NerdTreeBufName") | call <SNR>15_refreshRoot() | endif
" reload the window if directory is changed
augroup DIRCHANGE
    au!
    autocmd DirChanged global :NERDTreeCWD
augroup END

" auto close the NERDTree window if it is the only open window
autocmd WinEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" quit vim when nerdtree is the only window
" autocmd WinEnter * if winnr("$") == 1 && exists("t:NERDTreeBufName") && bufname("%") ==# t:NERDTreeBufName | quit | endif

" -- NETRW --

let g:netrw_altv = 1
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_dirhistmax = 0
let g:netrw_liststyle = 3
let g:netrw_preview = 1
let g:netrw_winsize = 50

" -- SHORTCUTS --

" find
map <C-f> /

" move current line up
nnoremap <C-k> :<C-u>move-2<CR>==
inoremap <C-k> <Esc>:<C-u>move-2<CR>==gi
vnoremap <C-k> :<C-u>move '<-3<CR>gv=gv

" move current line down
nnoremap <C-j> :<C-u>move+<CR>==
inoremap <C-j> <Esc>:<C-u>move+<CR>==gi
vnoremap <C-j> :<C-u>move '>+1<CR>gv=gv

