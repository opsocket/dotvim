# Vim Configuration

Here's the `.vim` folder I use 🐢

## Getting Started

Clone this repository in your `$HOME` folder

```shell
git clone git@gitlab.com:opsocket/dotvim.git ~/.vim
```

### Prerequisites

What things you need to install the software and how to install them

 * [Vim@8.0](https://www.vim.org/download.php)
